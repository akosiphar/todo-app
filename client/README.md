# Todo App

A simple Todo app built with [Vite and React] that allows you to manage your tasks effectively.

## Installation

1. Clone the repository.
2. Install dependencies using [npm].

## Usage

1. Run the app locally using `npm start`.
2. Access the app in your browser at [http://localhost:5173].
3. [Describe how to perform basic actions: add tasks, mark tasks as completed, delete tasks, etc.]

## Technologies Used

- Programming Languages: `JavaScript`
- Frameworks and Libraries: `React, Express, Node`
- Databases: `MongoDb`
- Development tools: `VS code, gitLab`
- Deployment platforms:

  for Front-end: [Vercel](https://vercel.com/)

  for Back-end: [Render](https://render.com/)

## Demo

Check out the live demo [here](https://todo-app-phi-bice.vercel.app/).

## Contributing

I'm still an amateur, that's why I'm open for comments or suggestions that will make my code better and cleaner. Thank you in Advance! 😊
