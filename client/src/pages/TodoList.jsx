import { useContext } from 'react'
import { TodoContext } from '../TodoContext'
import TodoCard from '../components/TodoCard'
import { ToastContainer } from 'react-toastify';

export default function TodoList() {
  const { todos } = useContext(TodoContext)


  return (
    <>
    {/* {isLoading 
    ?
      <div className="text-center">
        <ClipLoader size={30} color="#000000" loading={isLoading} />
        <p>Loading...</p>
      </div>
    : */}{
    todos.length === 0 
    ?
      <>
        <div className="card-container px-4 py-3 flex flex-wrap gap-5">
          <h1 className='text-3xl bg-red-300 p-3 rounded-xl'>No Todos. Add one!</h1>
        </div>
      </>
    :
      <>
      <div className="card-container px-4 py-3 flex flex-wrap gap-5">
        {todos.map((todo) => (
          <TodoCard 
            key={todo._id}
            title={todo.title}
            description={todo.description}
            id={todo._id}
            completed={todo.completed}
          />
        ))}
        <div>
        <ToastContainer
          position="top-center"
          autoClose={2000}
          hideProgressBar={false}
          newestOnTop={true}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover={false}
          theme="colored"
        />
      </div>
      </div>
      
      </>
    }
    </>
  )
}
