import { useContext, useState } from 'react'
import { TodoContext } from '../TodoContext'
import { toast } from 'react-toastify'

export default function TodoForm() {
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('') 
  const [isLoading, setIsLoading] = useState(false) 

  const { addTodo, setShowForm, showForm } = useContext(TodoContext)

  const handleClose = () => {
    setShowForm(!showForm)
  }

  const handleSubmit = async (e) => {
    e.preventDefault()
    setIsLoading(true)
    
    const newTodo = {title, description}
    
    try {
      const response = await fetch(`http://localhost:3000/todo/create`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(newTodo)
      })
      if(response.ok) {
        const data = await response.json()
        addTodo(data)
        
        setTitle('')
        setDescription('')
        toast.success('Added new Todo')
        handleClose()
        
      } else {
        toast.error('Something went wrong')
        console.error('Failed to Fetch')
      }
    } catch (error) {
      console.error('Failed to Fetch', error)
    }
    setIsLoading(false)
  }

  return (
    <>
      <div className={`fixed top-0 left-0 flex items-center justify-center h-screen w-full transition-all duration-300 z-10 ${showForm ? 'opacity-100' : 'opacity-0 pointer-events-none'}`}>
      <div className="bg-orange-200 rounded-lg p-6">
        <form onSubmit={handleSubmit} className="flex flex-col gap-4">
          <div className="flex justify-end">
            <div className="text-gray-500 cursor-pointer" onClick={handleClose}>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" />
              </svg>
            </div>
          </div>
          <input 
            type='text' 
            placeholder='Title' 
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            required
            className='py-2 px-3 rounded focus:outline-cyan-700' />
          <textarea 
            type='text' 
            placeholder='Description' 
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            className='py-2 px-3 rounded focus:outline-cyan-700' />

          {isLoading 
          ?
            <button className='bg-green-200 rounded p-1 hover:bg-green-400 transition-colors duration-300' disabled>Loading...</button>
          :
            <button className='bg-green-200 rounded p-1 hover:bg-green-400 transition-colors duration-300'>Submit</button>
          }
        </form>
      </div>
    </div>
    </>
  )
}
