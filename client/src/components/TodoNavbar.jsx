import { useContext } from 'react'
import TodoForm from './TodoForm'
import { TodoContext } from '../TodoContext'

export default function NavBar() {
  // const [showForm, setShowForm] = useState(false)
  const { showForm, setShowForm } = useContext(TodoContext)

  const toggleForm = () => {
    setShowForm(!showForm)
  } 


  return (
    <>
      <div className='flex justify-between items-center font-semibold'>
        <div className='text-2xl'>TODO APP</div>
        
        <button className='bg-red-300 p-2 rounded hover:bg-red-400 transition-all duration-150' onClick={toggleForm}>ADD TODO</button>
        
        {showForm ? <TodoForm /> : <TodoForm />}
      </div>
    </>
  )
}
