import { useContext, useEffect, useRef, useState } from 'react'
import { TodoContext } from '../TodoContext'

export default function TodoCard({ title, description, id, completed}) {
  // for clamping long text
  const [isClamp, setIsClamp] = useState(true)
  const [isMenuOpen, setIsMenuOpen] = useState(false)
  const [isCompleted, setIsCompleted] = useState(false)
  const descriptionRef = useRef(null)


  const { deleteTodo, checkTodo } = useContext(TodoContext)

  const handleDescriptionClick = () => {
    setIsClamp(!isClamp);
  };

  const handleCloseMenu = () => {
    setIsMenuOpen(false);
  };

  const handleDelete = async () => {
    deleteTodo(id);
  };

  const handleCheckboxChange = () => {
    checkTodo(id, completed);
  };
  

  useEffect(() => {
    // this is used to make the menu closed when clicked outside or pressed `ESC` key
    const element = descriptionRef.current;
    if(element && element.scrollHeight > element.offsetHeight) {
      setIsClamp(true)
    } else {
      setIsClamp(false)
    }

    const handleClickOutside = (e) => {
      if(!e.target.closest('.ellipsis-menu')) {
        handleCloseMenu();
      }
    }

    const handleKeyPress = (e) => {
      if(e.key === 'Escape') {
        handleCloseMenu()
      }
    }

    document.addEventListener('click', handleClickOutside)
    document.addEventListener('keydown', handleKeyPress)

    return() => {
      document.removeEventListener('click', handleClickOutside)
      document.removeEventListener('keydown', handleKeyPress)
    }

  },[description])


  return (
    <div className="card card-body w-60 bg-orange-100 p-3 shadow-md rounded relative break-words h-fit">

    {/* TITLE AND DESCRIPTION */}
      <h4 className={`card-title border-b border-gray-500 mb-1 ${completed ? 'line-through' : ''}`}>{title}</h4>
      <div className="card-text"
      >
        <span className={` ${isClamp ? 'line-clamp-2' : ''} `}
        ref={descriptionRef}
        >
        {description}
        </span>
        {isClamp && (
          <div className='text-blue-500 text-base cursor-pointer' onClick={handleDescriptionClick}>
          See more
          </div>
        )}
        {!isClamp && description.length > 40 && (
          <div className='text-blue-500 text-base cursor-pointer' onClick={handleDescriptionClick}>
          See less
          </div>
        )}
      </div>

    {/* CHECKBOX */}
      <div>
        <label className='flex items-center gap-1 justify-end cursor-pointer'>
          <input type="checkbox" className={`w-4 h-4`} checked={completed} onChange={handleCheckboxChange} />
          <div>Completed</div>
        </label>
      </div>

    {/* ELLIPSIS MENU */}
      <div className='ellipsis-menu text-xl font-bold absolute top-0 right-3 cursor-pointer' onClick={() => setIsMenuOpen(true)}>...</div>
      <div className={`ellipsis-menu flex flex-col py-2 shadow-sm bg-white absolute top-8 right-0 rounded ${isMenuOpen ? '' : 'hidden'} `}>
        <button className='hover:bg-slate-500 px-3 py-1'>Edit</button>
        <button className='hover:bg-slate-500 px-3 py-1' onClick={handleDelete}>Delete</button>
      </div>

    </div>
  )
}
