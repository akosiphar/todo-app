import { createContext, useEffect, useState } from "react";
import { toast } from "react-toastify";

export const TodoContext = createContext({ todos: [], loading: false})

export const TodoProvider = ({ children }) => {
  const [todos, setTodos] = useState([])
  const [showForm, setShowForm] = useState(false)

  // GETTING ALL TODOS
  const fetchTodos = async () => {
    
    try {
      const response = await fetch(`http://localhost:3000/todo`);
      if (response.ok) {
        const data = await response.json()
        setTodos(data)
      } else {
        console.error('Failed to Fetch')
      }
    } catch (error) {
      console.error('Failed to Fetch', error)
    }
    
    // setIsLoading(false)
  }


  // DELETING TODO
  const deleteTodo = async (id) => {
    try {
      await fetch(`${import.meta.env.VITE_RENDER_LINK}/todo/${id}`, {
        method: 'DELETE',
      });
      
      // Remove the deleted todo from the todos state
      setTodos((prevTodos) => prevTodos.filter((todo) => todo._id !== id));
      toast.warning('Todo Deleted', {
        theme: 'dark'
      })
    } catch (error) {
      console.error('Error deleting todo:', error);
      // Handle the error, such as showing an error message to the user
    }

  };


  // ADDING TODO
  const addTodo = (todo) => {
    setTodos([...todos, todo])
  }


  // CHECKBOX of TODO
  const checkTodo = async (id, completed) => {
    try {
      await fetch(`${import.meta.env.VITE_RENDER_LINK}/todo/${id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ completed: !completed }), // Toggle the completed property
      });
  
      // Update the todos state to reflect the toggled completion status
      setTodos((prevTodos) =>
        prevTodos.map((todo) => {
          if (todo._id === id) {
            return { ...todo, completed: !completed };
          }
          return todo;
        })
        );
        toast.success(`Success updating`)
    } catch (error) {
      toast.error('Something went wrong')
      console.error('Error updating todo:', error);
      // Handle the error, such as showing an error message to the user
    }
  };
  
  
  useEffect(() => {
    fetchTodos()
  },[])


  return (
    <TodoContext.Provider value={{ todos, setTodos, showForm, setShowForm, addTodo, deleteTodo, checkTodo }}>
      {children}
    </TodoContext.Provider>
  )
}
