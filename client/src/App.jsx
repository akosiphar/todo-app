import { TodoProvider } from "./TodoContext"
import NavBar from "./components/TodoNavbar"
import TodoList from "./pages/TodoList"

function App() {

  return (
    <>
    <TodoProvider>
    <div className="bg-slate-50 h-screen">
      <div className="px-4 py-3">
      <NavBar />
      </div>
      <TodoList />
    </div>
    </TodoProvider>
    </>
  )
}

export default App
