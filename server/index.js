// dependencies
const mongoose = require('mongoose');
const express = require('express');
const cors = require('cors')
const helmet = require('helmet')
// const dotenv = require('dotenv').config()

// if(dotenv.error) {
//   console.error('Error loading .env file', dotenv.error);
//   process.exit(1);
// }

// * Models
const todos = require('./model/Todos')


// creating server/application
const app = express();

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}))
app.use(helmet())

// * Routes
const todoRoutes = require('./Routes/TodoRoutes.js');
app.use("/todo", todoRoutes);

// Connecting to Mongoose/MongoDB
mongoose.connect('mongodb+srv://admin:admin@batch230.rr5eflw.mongodb.net/todoList?retryWrites=true&w=majority', {
  useNewUrlParser: true,
  useUnifiedTopology: true,})

  // model.syncIndexes() is used to synchronize your defined indexes in the model to the database
  .then(() => {
    todos.syncIndexes()
    .then(() => console.log('Indexes synchronized successfully'))
  })
  .catch((error) => {
    console.error.apply('Failed to synchronize indexes', error)
  })


// mongoose.connection.on("error", () => console.log("Connection Error:"));
mongoose.connection.once("open", () => console.log("Now Connected to Your Database"));




// * To connect and listen to the port
app.listen(process.env.PORT || 3000, () => {
  console.log(`API is now online! PORT - ${process.env.PORT || 3000} - using express`)
})
