const express = require('express');
const router = express.Router();
const todoControllers = require('../controllers/TodoController.js')
const rateLimit = require('express-rate-limit')

const todoRequestLimit = rateLimit({
  windowMs: 60 * 60 * 1000, // 5 minutes
  max: 3, // limit each IP to 5 requests per windowMs
  message: 'Too many requests from this IP, please try again in 5 minutes'
})

router.get('/', todoControllers.getAllTodo);
router.get('/:id', todoControllers.getTodo);
router.post('/create', todoRequestLimit, todoControllers.createTodo);
router.put('/:id', todoControllers.updateTodo)
router.delete('/:id', todoControllers.deleteTodo);

module.exports = router;