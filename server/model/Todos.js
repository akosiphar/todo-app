const mongoose = require('mongoose')

const todoSchema = new mongoose.Schema(
  {
    id: {
      type: Number,
      // required: true,
      unique: true,
      },
    title: {
      type: String,
      required: true,
      // unique: true
    },
    description: String,
    dueDate: {
      type: Date,
      default: Date.now()
    },
    completed: {
      type: Boolean,
      default: false
    }
  }
);

todoSchema.virtual('formattedDueDate').get(function() {
  return this.dueDate.toLocaleDateString();
});

todoSchema.pre('save', async function(next) {
  const lastTodo = await this.constructor.findOne({},{}, { sort: { id: -1} });
  if(lastTodo) {
    this.id = lastTodo.id + 1
  } else {
    this.id = 1
  }
})

// const Todo = mongoose.model('Todo', todoSchema)

// module.exports = Todo;

module.exports = mongoose.model("Todo", todoSchema)