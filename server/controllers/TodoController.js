const mongoose = require('mongoose')
const Todo = require('../model/Todos.js')


// module.exports.createTodo = (req, res) => {

//   Todo.findOne({title: req.body.title})
//   .then(result => {
//     if(result != null && result.title == req.body.title) {
//       return res.send(false)
//     } else {
//       let todo = new Todo({
//         title: req.body.title,
//         description: req.body.description,
//         dueDate: req.body.dueDate,
//         completed: req.body.completed
//       })
      
//       return todo.save()
//       .then(result => res.send(result))
//       .catch(error => res.send(error))
//     }
//   })

// }

const todoController = {
  // Creating a new TODO
  createTodo: async (req, res, next) => {
    try {

    const { title, description, dueDate, completed } = req.body;

    const todo = new Todo({
      title,
      description,
      // dueDate: date,
      completed,
    });

    const newTodo = await todo.save();

    res.status(201).json(newTodo);
    } catch (error) {
      next(error)
      // return res.status(404).json({message: error.message})
    }
  },

  // Getting single Todo
  getTodo: async (req, res, next) => {
    try {
      const todo = await Todo.findById(req.params.id);
      if (!todo) {
        return res.status(404).json({ message: 'Todo not found' });
      }
      res.json(todo);
    } catch (err) {
      next(err);
    }
  },

  // Getting All Todo
  getAllTodo: async (req, res, next) => {
    try {
      const todos = await Todo.find({});
      res.json(todos);
    } catch (error) {
      next(error)
      return res.status(404).json({message: error.message})
    }
  },

  // Updating Todo
  updateTodo: async (req, res, next) => {
    try {
      const todo = await Todo.findByIdAndUpdate(req.params.id,
        {
          title: req.body.title, 
          description: req.body.description,
          dueDate: req.body.dueDate, 
          completed: req.body.completed
        },
        {new: true}
      );
      if(!todo) {
        return res.status(404).json({ message: 'Todo not Found'})
      }

      res.json(todo)

    } catch (error) {
      next(error)
      return res.status(404).json({message: error.message})
    }
  },

  // Deleting Todo
  deleteTodo: async (req, res, next) => {
    try {
      const todo = await Todo.findByIdAndDelete(req.params.id);
      if(!todo) {
        return res.status(404).json({ message: 'Todo not Found'});
      }
      res.json({ message: 'Todo deleted'});
    } catch (error) {
      next(error) 
      return res.status(404).json({ message: error.message })
    }
  }
}

module.exports = todoController;